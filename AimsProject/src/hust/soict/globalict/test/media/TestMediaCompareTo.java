package hust.soict.globalict.test.media;

import java.util.ArrayList;

import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;

public class TestMediaCompareTo 
{
	public static void main(String[] args) 
	{
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation",19.95f, 87,  "Roger Allers");
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", 24.95f, 124, "George Lucas");
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladin","Animation",18.99f,90,"John Musker");
		
		ArrayList arr = new ArrayList();
		arr.add(dvd2);
		arr.add(dvd1);
		arr.add(dvd3);
		
		java.util.Iterator iterator = arr.iterator();
		while(iterator.hasNext())
		{
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		
		java.util.Collections.sort((java.util.List)arr);
		
		iterator = arr.iterator();
		System.out.println("sorted");
		while(iterator.hasNext())
		{
			System.out.println(((DigitalVideoDisc)iterator.next()).getTitle());
		}
		
		Track track1 = new Track("a", 50);
		Track track2 = new Track("b", 60);
		Track track3 = new Track("c", 70);
		Track track4 = new Track("d", 80);
		
		CompactDisc a = new CompactDisc("CD1"); a.addTrack(track4);a.addTrack(track2);a.addTrack(track1);
		CompactDisc b = new CompactDisc("CD2"); b.addTrack(track1);b.addTrack(track2);
		CompactDisc c = new CompactDisc("CD3"); c.addTrack(track3);c.addTrack(track2);c.addTrack(track1);
		CompactDisc d = new CompactDisc("CD4"); d.addTrack(track1);d.addTrack(track3);
		
		ArrayList arr2 = new ArrayList();
		arr2.add(a);
		arr2.add(b);
		arr2.add(c);
		arr2.add(d);
		
		iterator = arr2.iterator();
		while(iterator.hasNext())
		{
			System.out.println(((CompactDisc)iterator.next()).getTitle());
		}
		
		java.util.Collections.sort((java.util.List)arr2);
		iterator = arr2.iterator();
		System.out.println("sorted");
		while(iterator.hasNext())
		{
			System.out.println(((CompactDisc)iterator.next()).getTitle());
		}
		
		
		
	}

}
