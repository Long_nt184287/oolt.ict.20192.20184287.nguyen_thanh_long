package hust.soict.globalict.test.media;

import java.util.ArrayList;

import hust.soict.globalict.aims.media.Book;

public class TestBook 
{

	public static void main(String[] args)
	{
		ArrayList<String> authors = new ArrayList<String>();
		authors.add("User");
		authors.add("Poet");
		Book book = new Book("The Book", "Scientific", authors, 20);
		book.setContent("This is a si is it is");
		book.processContent();
		
		System.out.println(book);
	}

}
