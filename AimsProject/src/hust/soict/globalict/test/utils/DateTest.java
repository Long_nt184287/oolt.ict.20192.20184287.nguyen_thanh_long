package hust.soict.globalict.test.utils;
import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {

	public static void main(String[] args) 
	{
		//test method!!!
		MyDate dateA = new MyDate();
		MyDate dateB = new MyDate(12, 5, 2000);
		MyDate dateC = new MyDate(18, 6, 2800);
		MyDate dateD = new MyDate("March 13th 2020");
		
		
		dateA.printDate();
		dateB.printDate();
		dateC.printDate();
		dateD.printDate();
		
		System.out.println("_____________");
		
		dateA.accept("August 16th 2020");
		dateA.printDate();
		
		System.out.println("_____________");
		
		MyDate[] dates = {dateA, dateB, dateC, dateD};
		DateUtils.PrintArrOfDate(dates);
		System.out.println("_____________");
		DateUtils.Sort(dates);
		DateUtils.PrintArrOfDate(dates);
	}

}
