package hust.soict.globalict.aims;


import hust.soict.globalict.aims.order.Order;
import hust.soict.globalict.thread.MemoryDaemon;

import java.util.ArrayList;
import java.util.Scanner;

import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;;


public class Aims 
{
	//create stuff management :v

	ArrayList<DigitalVideoDisc> DVDList = new ArrayList<DigitalVideoDisc>();
	ArrayList<Book> bookList = new ArrayList<Book>();
	ArrayList<Track> trackList = new ArrayList<Track>();
	
	public void printMediaList(ArrayList<Media> arr)
	{
		int i = 1;
		for(Media a : arr)
		{
			System.out.print(i + ". " + a.getTitle() + "\t");
			i++;
		}
	}
	
	ArrayList<Order> listOfOrder = new ArrayList<Order>();
	Order currentOrder = null;
	
	public void createNewOrder()
	{
		currentOrder = new Order();
		listOfOrder.add(currentOrder);
	}
	
	public static void showMenu()
	{
		
		System.out.println("Order Management Application: ");
		System.out.println("_____________________________");
		System.out.println("1. Create new order");
		System.out.println("2. Add item to the order");
		System.out.println("3. Delete item by id");
		System.out.println("4. Display the items list of you order");
		System.out.println("0.exit");
		
	}
	
	public static void main(String[] args) 
	{
		
		Aims aims = new Aims();
		MemoryDaemon md = new MemoryDaemon();
		Thread thread = new Thread(md);
		thread.setDaemon(true);
		thread.start();
		//create stuff 
		
		
		//default param : string title, string category, float cost, int length, string director
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation",19.95f, 87,  "Roger Allers");
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", 24.95f, 124, "George Lucas");
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladin","Animation",18.99f,90,"John Musker");
		
		ArrayList<String> myAuthor = new ArrayList<String>();
		myAuthor.add("Someone1");
		myAuthor.add("Someone2");
		
		Book book1 = new Book("Little Prince", "Telltale", myAuthor, 33.33f);
		
		
		aims.DVDList.add(dvd1);
		aims.DVDList.add(dvd2);
		aims.DVDList.add(dvd3);
		aims.bookList.add(book1);
		
		Track track1 = new Track("a", 50);
		Track track2 = new Track("b", 60);
		Track track3 = new Track("a", 50);
		
		aims.trackList.add(track1);
		aims.trackList.add(track2);
		aims.trackList.add(track3);
		
		
		//TestMethod
		int c;
		boolean exit = false;
		while(!exit)
		{
			showMenu();
			Scanner keyboard = new Scanner(System.in);
			c = keyboard.nextInt();
			switch(c)
			{
			case 1: aims.createNewOrder(); break;
			case 2: 
			{
				aims.currentOrder.addMedia(aims.addItem(aims));break;
			}
			case 3:
			{
				System.out.println("Pls input the id of the Media u wanna delete!");
				int d = keyboard.nextInt();
				aims.currentOrder.removeMedia(d);
				break;
			}
			case 4:
			{
				aims.currentOrder.printOrder();
				break;
								
			}
			case 0: {exit = true; break;}
			}
		}	
	}
	
	
	public Media addItem(Aims aim)
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("\n You want to buy : 1.DVD 2.CD 3.Book :\n");
		int c = keyboard.nextInt();
		switch(c)
		{
		case 1 : 
			{
				int i = 1;
				for(DigitalVideoDisc a : aim.DVDList)
				{
					System.out.print(i + ". " + a.getTitle() + "\t");
					i++;
				}
				
				
				int e = keyboard.nextInt();
				if(e-1 < 0 || e-1 > aim.DVDList.size())
				{
					System.out.println("There's no such DVD available! \n");
					return null;
				}
				
				System.out.println("Do you want to play this CD? 1:y 0:n \n");
				int yn = keyboard.nextInt();
				if(yn == 1) 
				{ 
						try 
						{
							aim.DVDList.get(e-1).play();
						}
						catch (PlayerException ex)
						{
							ex.printStackTrace();
						}
				} 
				return (aim.DVDList.get(e-1));
			}
			
		case 2:	
			{
				CompactDisc cd = new CompactDisc();
				boolean check = true;
				while(check)
				{
					System.out.println("Select the track u wanna add to ur CD: \n");
					int count = 1;
					for(Track a : aim.trackList)
					{
						System.out.print(count + ". " + a.getTitle() +"\t");
						count++;
					}
					System.out.print("0. exit\n");
					
					int input = keyboard.nextInt();
					if(input == 0) {check = false; break;}
					else 
					{
						cd.addTrack(aim.trackList.get(input-1));
						
					}
				}
				System.out.println("Do you want to play this CD? 1:y 0:n \n");
				int yn = keyboard.nextInt();
				if(yn == 1) 
				{
					try
					{
						cd.play();
					}
					catch (PlayerException ex)
					{
						ex.printStackTrace();
					}
				} 
				return cd; 
			}
			
		case 3:
			{
				int i = 1;
				for(Book a : aim.bookList)
				{
					System.out.print(i + ". " + a.getTitle() + "\t");
					i++;
				}
				
				int e = keyboard.nextInt();
				if(e-1 < 0 || e-1 > aim.bookList.size())
				{
					System.out.println("There's no such book available! \n");
					return null;
				}
				return (aim.bookList.get(e-1));
			}
			
		default : return null;
		}
	}

}
