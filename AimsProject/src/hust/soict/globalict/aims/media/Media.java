package hust.soict.globalict.aims.media;

public abstract class Media extends Object
{
	protected String title;
	
	protected String category;
	
	protected float cost;
	
	protected int id;
	
	public Media()
	{
	}
	
	public Media(String title) 
	{
		this.title = title;
	}
	
	public Media(String title, String category) 
	{
		this.title = title;
		this.category = category;
	}
	
	public Media(String title, String category, float cost) 
	{
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	


	public String getTitle() {
		return title;
	}


	


	public String getCategory() {
		return category;
	}


	


	public float getCost() {
		return cost;
	}


	
	
	public void printInfo()
	{
		
	}
	
	public boolean equals(Object obj) 
	{
        try 
        {
            if (obj.getClass() == this.getClass()) 
            {
                Media media = (Media) obj;
                if (media.getCost() == this.getCost() && (media.getTitle() == this.getTitle()))
                    return true;
                else
                    return false;
            } 
            else
                return false;
        } 
        catch (NullPointerException e1) 
        {
            e1.printStackTrace();
        } 
        catch (ClassCastException e2) 
        {
            e2.printStackTrace();
        } 
        finally 
        {
            return false;
        }
    }
	
	
	

}
