package hust.soict.globalict.aims.media;

import hust.soict.globalict.aims.PlayerException;

public class Track extends Object implements Playable 
{
	private String title;
	private int length;
	
	public boolean Equals(Track track)
	{
		return(this.getLength() == track.getLength() && this.getTitle() == track.getTitle());
	}
	
	public Track(String title, int length)
	{
		this.title = title;
		this.length = length;
	}
	
	public String getTitle() {
		return title;
	}
	public int getLength() {
		return length;
	}
	
	public void play() throws PlayerException
	{
		if(this.getLength() <= 0)
		{
			System.err.println("Track length is less than 0!");
			throw (new PlayerException());
		}
		System.out.println("Playing track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength());
	}


}
