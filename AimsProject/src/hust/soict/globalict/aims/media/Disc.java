package hust.soict.globalict.aims.media;

public class Disc extends Media
{
	protected int length;
	
	protected String director;

	public int getLength() {
		return length;
	}

	public String getDirector() {
		return director;
	}
	
	//list of param : title category cost length director.
	
	
	public Disc()
	{
		super();
	}
	public Disc(String title) 
	{
		super(title);
	}
	public Disc(String title, String category) 
	{
		super(title, category);
	}
	
	public Disc(String title, String category, float cost) 
	{
		super(title, category, cost);
	}
	public Disc(String title, String category, float cost, int lenght) 
	{
		super(title, category, cost);
		this.length = lenght;
	}
	public Disc(String title, String category, float cost, int lenght, String director ) 
	{
		super(title, category, cost);
		this.director = director;
		this.length = lenght;
	}
	
}
