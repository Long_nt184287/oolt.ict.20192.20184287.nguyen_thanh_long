package hust.soict.globalict.aims.media;

import java.util.ArrayList;

import hust.soict.globalict.aims.PlayerException;

public class CompactDisc extends Disc implements Comparable
{

	private String artist;
	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();
	
	
	public int getLength()
	{
		return length;
	}
	
	public String getArtist()
	{
		return artist;
	}
	
	public ArrayList<Track> getTrack()
	{
		return tracks;
	}
	
	public CompactDisc()
	{
		super();
	}
	public CompactDisc(String title) 
	{
		super(title);
	}
	public CompactDisc(String title, String category) 
	{
		super(title, category);
	}
	
	public CompactDisc(String title, String category, float cost) 
	{
		super(title, category, cost);
	}
	public CompactDisc(String title, String category, float cost, int lenght) 
	{
		super(title, category, cost, lenght);
	}
	public CompactDisc(String title, String category, float cost, int lenght, String director ) 
	{
		super(title, category, cost, lenght, director);
	}
	

	
	
	public void addTrack(Track track)
	{
		if(tracks.isEmpty()) {tracks.add(track); this.length += track.getLength();}
		else
		{
			for(Track a : tracks)
			{
				if(a.getTitle() == track.getTitle())
				{
					System.out.println("This track has already been in the CD");
					return;
				}
			}
			tracks.add(track);
			System.out.println("Added");
			this.length += track.getLength();
			return;
		}
		
			
	}
	
	public void removeTrack(Track track)
	{
		if(!tracks.remove(track))
		{
			System.out.println("This track isn't in the CD");
		}
	}
	
	public void play() throws PlayerException
	{
		if(this.getLength() <= 0)
		{
			System.err.println("CD has length less than 0");
		}
		System.out.println("CD length: " + this.getLength());
		for(Track a : tracks)
		{
			try
			{
				a.play();
			}
			catch (PlayerException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	public void printInfo()
	{
		int c = 0;
		System.out.print(". This is a custom CD i guess.. Including:");
		for(Track a : tracks)
		{
			c++;
			System.out.print(c + ". " + a.getTitle() + ";\t");
		}
		System.out.print("\n");
		
	}
	
	public int compareTo(Object m)
	{
		if(((Integer)this.tracks.size()).compareTo(((CompactDisc)m).getTrack().size()) == 0)
		{
			return ((Integer)this.getLength()).compareTo(((CompactDisc) m).getLength());
		}
		
		else return ((Integer)this.tracks.size()).compareTo(((CompactDisc)m).getTrack().size());
	}
	
}
