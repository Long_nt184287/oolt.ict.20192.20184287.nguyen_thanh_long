package hust.soict.globalict.aims.order;
import java.util.ArrayList;

import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

public class Order 
{
	public static final int MAX_NUMBER_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 10;
	private static int nbOrders;
	private int id;
	
	public Order()
	{
		nbOrders++;
		if(nbOrders > MAX_LIMITTED_ORDERS)
		{
			System.out.println("You cant order more!");
			
		}
		id = nbOrders;
	}
	
	
	private MyDate dateOrdered = new MyDate();
	private ArrayList <Media> itemsOrdered = new ArrayList<Media>();
	
	public void addMedia(Media media)
	{
		if(itemsOrdered.size() <10 )
		{
			itemsOrdered.add(media);
		}
		else System.out.println("You have ordered the maximum number of media!");
	}
	
	public void removeMedia(int index)
	{
		if(itemsOrdered.size() > index && index > 0)
		{
			itemsOrdered.remove(index);
		}
		else System.out.println("There is no such media in your order!");
		
	}
	
	
	public void totalCost()
	{
		float cost = 0;
		for(Media a : itemsOrdered)
		{
			cost += a.getCost();
		}
		
		System.out.println("Your items cost " + cost + "$");
	}
	
	
	public void printOrder()
	{
		int i = 1;
		System.out.println("************Order***********");
		System.out.print("Date: "); dateOrdered.printDate();
		System.out.println("Ordered Items:");
		for(Media a : itemsOrdered)
		{
			System.out.print(i);
			a.printInfo();
			i++;
		}
		totalCost();
		System.out.println("*****************************");
		
		
	}
	
	/*
	public void getLuckyItem()
	{
		int i = itemsOrdered.size();
		int luckyitem = (int)(Math.random()*i);
		Media LuckyItem = itemsOrdered.get(luckyitem);
		LuckyItem.setCost(0f);
		itemsOrdered.set(luckyitem, LuckyItem);
		System.out.println("Your luckyitem is " + itemsOrdered.get(luckyitem).getTitle());
	}
	
	public DigitalVideoDisc FreeDisc(DigitalVideoDisc origin)
	{
		DigitalVideoDisc disc = new DigitalVideoDisc();
		disc.setCategory(origin.getCategory());
		disc.setDirector(origin.getDirector());
		disc.setLenght(origin.getLenght());
		disc.setTitle(origin.getTitle());
		disc.setCost(0);
		
		return disc;
				
	}
	*/
	

}
