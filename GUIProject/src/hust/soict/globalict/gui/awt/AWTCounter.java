package hust.soict.globalict.gui.awt;
import java.awt.*;
import java.awt.event.*;


public class AWTCounter extends Frame implements ActionListener 
{
	private Label lbCount;
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;
	
	public AWTCounter ()
	{
		setLayout(new FlowLayout());
		
		lbCount = new Label("Counter");
		add(lbCount);
		
		tfCount = new TextField(count + "", 10);
		tfCount.setEditable(false);
		add(tfCount);
		
		btnCount = new Button("Count");
		add(btnCount);
		
		btnCount.addActionListener(this);
		
		
		setTitle("AWT Counter");
		setSize(250, 100);
		
		setVisible(true);
	}
	
	
	public static void main(String[] args)
	{
		
		AWTCounter app = new AWTCounter();
	}
	
	public void actionPerformed(ActionEvent e)
	{
		++count;
		
		tfCount.setText(count + "");
	}

}
