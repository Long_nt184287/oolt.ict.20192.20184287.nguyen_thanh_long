package hust.soict.globalict.lab02;
import java.util.Scanner;
public class DisplayNumberofDays 
{
	Scanner keyboard = new Scanner(System.in);
	public int month()
	{
		String month;
		System.out.println("Input the name of the month :");
		while(true)
		{
			month = keyboard.nextLine();
			switch(month)
			{
			case "Jan" : case "Jan." : case "January" : case "1" : return 1;
			case "Feb" : case "Feb." : case "February" : case "2" : return 2;
			case "Mar" : case "Mar." : case "March" : case "3" : return 3;
			case "Apr" : case "Apr." : case "April" : case "4" : return 4;
			case "May" : case "5" : return 5;
			case "Jun" : case "Jun." : case "June" : case "6" : return 6;
			case "Jul" : case "Jul." : case "July" : case "7" : return 7;
			case "Aug" : case "Aug." : case "August" : case "8" : return 8;
			case "Sep" : case "Sep." : case "September" : case "9" : return 9;
			case "Oct" : case "Oct." : case "October" : case "10" : return 10;
			case "Nov" : case "Nov." : case "November" : case "11" : return 11;
			case "Dec" : case "Dec." : case "December" : case "12" : return 12;
			}
			System.out.println("Wrong format detected, please reenter the month :");
		}
	}
	
	public boolean leapyearCheck()
	{
		int year = 0;
		System.out.println("Input the year :");
		
		while (true)
		{
			Scanner kb = new Scanner(System.in);
			try 
			{
				year = kb.nextInt();
			}
			catch(java.util.InputMismatchException ex)
			{
				System.out.println("Wrong format detected, please reenter the year :");
				continue;
			}
			if(year < 1000 || year > 9999)
			{
				System.out.println("Wrong format detected, please reenter the year :");
				continue;
			}
			break;
		}
		
		if (year%4 != 0) return false;
		else if (year%100 != 0) return true;
		else if (year%400 != 0) return false;
		else return true;
	}
	
	public static void main(String[] Args)
	{
		DisplayNumberofDays program = new DisplayNumberofDays();
		int month = program.month();
		boolean lyCheck = program.leapyearCheck();
		switch (month)
		{
		case 1 : case 3 : case 5 : case 7 : case 8 : case 10 : case 12 : System.out.println("this month has 31 days!");
		case 4 : case 6 : case 9 : case 11 : System.out.println("this month has 30 days!");
		case 2 : if(lyCheck) System.out.println("this month has 29 days!");
					else System.out.println("this month has 28 days!");
		}
	}
}
