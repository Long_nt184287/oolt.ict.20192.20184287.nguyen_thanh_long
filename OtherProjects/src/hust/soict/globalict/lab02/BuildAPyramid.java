package hust.soict.globalict.lab02;
import java.util.Scanner;
public class BuildAPyramid 
{
	public static void main(String[] Args)
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input the height of the pyramid : ");
		int height = keyboard.nextInt();
		
		for(int i = 0; i < height; i ++)
		{
			for(int j = height - i; j > 0; j--)
			{
				System.out.print(" ");
			}
			for(int k = 0; k <2*i + 1; k++)
			{
				System.out.print("*");
			}
			System.out.print("\n");
		}
		

	}
}
