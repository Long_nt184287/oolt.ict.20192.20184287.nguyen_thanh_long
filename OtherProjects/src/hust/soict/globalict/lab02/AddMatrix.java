package hust.soict.globalict.lab02;
import java.util.Scanner;
public class AddMatrix 
{
	public static void main(String[] args)
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Input the rank of the matrix : ");
		int rank = keyboard.nextInt();
		
		int[][] matrixA = new int[rank][rank];
		int[][] matrixB = new int[rank][rank];
		int[][] matrixAns = new int[rank][rank];
		
		System.out.println("Input the matrix A : ");
		for(int i = 0; i < rank; i++)
		{
			for(int j = 0; j < rank; j++)
			{
				matrixA[i][j] = keyboard.nextInt();
			}
		}
		System.out.println("Input the matrix B : ");
		for(int i = 0; i < rank; i++)
		{
			for(int j = 0; j < rank; j++)
			{
				matrixB[i][j] = keyboard.nextInt();
			}
		}
		
		for(int i = 0; i < rank; i++)
		{
			for(int j = 0; j < rank; j++)
			{
				matrixAns[i][j] = matrixA[i][j] + matrixB[i][j];
			}
		}
		
		System.out.println("The answer is : ");
		for(int i = 0; i < rank; i++)
		{
			for(int j = 0; j < rank; j++)
			{
				System.out.print(matrixAns[i][j] + "\t");
			}
			System.out.print("\n");
		}
	}
	

}
