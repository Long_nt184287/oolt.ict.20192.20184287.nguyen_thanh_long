package hust.soict.globalict.lab02;
import java.util.Scanner;
public class InPutFromKeyboard 
{
	public static void main(String[] arg)
	{
		Scanner keyboard = new Scanner(System.in);
		System.out.println("What's your name?");
		String strName = keyboard.nextLine();
		System.out.println("What's your age?");
		int age = keyboard.nextInt();
		System.out.println("What's your height?");
		float height = keyboard.nextFloat();
		
		System.out.println("Mrs/Ms "+ strName + ", " + age + " years old. Your height is " + height  );
		
	}

}
