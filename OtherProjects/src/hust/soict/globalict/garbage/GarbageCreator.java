package hust.soict.globalict.garbage;
import java.io.File;
import java.util.Scanner;;

public class GarbageCreator 
{
	public GarbageCreator()
	{
		try 
		{
			String filePath = "D:\\New Text Document.txt";
	        File file = new File(filePath);
	        Scanner scanner = new Scanner(file);
	        String string = "";

	        long start = System.currentTimeMillis();

	        while (scanner.hasNextLine())
	        {
	            string += (scanner.nextLine());
	        }
	        System.out.println((System.currentTimeMillis() - start));

		}
		
		catch (Exception e)
		{
			System.out.println("An error occurred!");
		}
		
	}
	
	
	
}
