package hust.soict.globalict.garbage;

import java.io.File;
import java.util.Scanner;

public class NoGarbageCreator 
{
	public NoGarbageCreator()
	{
		try 
		{
			String filePath = "D:\\New Text Document.txt";
	        File file = new File(filePath);
	        Scanner scanner = new Scanner(file);
	        StringBuilder string = new StringBuilder();

	        long start = System.currentTimeMillis();

	        while (scanner.hasNextLine())
	        {
	            string.append(scanner.nextLine());
	        }
	        System.out.println((System.currentTimeMillis() - start));

		}
		
		catch (Exception e)
		{
			System.out.println("An error occurred!");
		}
		
	}

}
