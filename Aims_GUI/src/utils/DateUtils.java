package utils;
import java.util.*;
import java.lang.*; 
import java.io.*; 
public class DateUtils 
{
	// 1 means a after b
	// -1 means a before b
	// 0 means a is b
	public static int Compare(MyDate a, MyDate b)
	{
		if(a.getYear() > b.getYear())
		{
			return 1;
		}
		
		else if(a.getYear() < b.getYear())
		{
			return -1;
		}
		else
		{
			if(a.getMonth() > b.getMonth())
			{
				return 1;
			}
			
			else if(a.getMonth() < b.getMonth())
			{
				return -1;
			}
			else
			{
				if(a.getDay() > b.getDay())
				{
					return 1;
				}
				else if(a.getDay() < b.getDay())
				{
					return -1;
				}
				else
				{
					return 0;
				}
			}
		}
	}
	
	public static void CompareDate(MyDate a, MyDate b)
	{
		int result = Compare(a, b);
				
		switch(result)
		{
			case 1: System.out.println("The first date is after the second date!");break;
			case 2:	System.out.println("The first date is before the second date!");break;
			case 3:	System.out.println("The first date and the second date are equal!");break;
		}
	}
	
	public static void Sort(MyDate[] input)
	{
		int length = input.length;
		for(int i = 0; i < length-1; i++)
		{
			for(int j = 0 ; j < length - i - 1 ; j++)
			{
				if(Compare(input[j], input[j+1]) == 1)
				{
					MyDate tmp = input[j];
					input[j] = input[j+1];
					input[j+1] = tmp;
				}
			}
		}
	}
	
	public static void PrintArrOfDate(MyDate[] input)
	{
		for(int a = 0; a < input.length; a++)
		{
			input[a].printDate();
		}
	}

}
