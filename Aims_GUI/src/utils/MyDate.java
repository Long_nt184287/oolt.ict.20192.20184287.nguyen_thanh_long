package utils;
import java.time.LocalDate;
public class MyDate 
{
	private int day;
	private int month;
	private int year;
	
	
	//getter and setter
	public int getDay() {
		return day;
	}

	public void setDay(int day) {
		this.day = day;
	}

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	
	//constructor
	public MyDate()
	{
		java.time.LocalDate.now().getDayOfMonth();
		
		//i dunno how to get current date urrghhh!!!
		day = java.time.LocalDate.now().getDayOfMonth();
		month = java.time.LocalDate.now().getMonthValue();
		year = java.time.LocalDate.now().getYear();
		
	}
	
	public MyDate(int day, int month, int year)
	{	
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	//only case for correct input :<<
	//example February 18th 2019
	public MyDate(String string)
	{
		//Consider using space as delimiter!!!
		String arrOfString[] = string.split(" ", -1);
		switch(arrOfString[0])
		{
		case "January" : month = 1; break;
		case "February" : month = 2; break;
		case "March" : month = 3; break;
		case "April" : month = 4; break;
		case "May" : month = 5; break;
		case "June" : month = 6; break;
		case "July" : month = 7; break;
		case "August" : month = 8; break;
		case "September" : month = 9; break;
		case "October" : month = 10; break;
		case "November" : month = 11; break;
		case "December" : month = 12; break;
		default : System.out.println("Invalid input!");
		
		}
		year = Integer.parseInt(arrOfString[2]);
		day = Integer.parseInt(arrOfString[1].substring(0, 2));
	}
	
	
	public MyDate(String day, String month, String year)
	{
		System.out.println("This feature is currently not availble!");
	}
	
	
	public void printDate()
	{
		System.out.println(month + "/" + day + "/" + year);
	}

	// this method feels like the constructor and i know writing code
	// like this is not recommended
	// but i dunno how to fix dis!!!!
	public void accept(String string)
	{
		String arrOfString[] = string.split(" ", -1);
		switch(arrOfString[0])
		{
		case "January" : month = 1; break;
		case "February" : month = 2; break;
		case "March" : month = 3; break;
		case "April" : month = 4; break;
		case "May" : month = 5; break;
		case "June" : month = 6; break;
		case "July" : month = 7; break;
		case "August" : month = 8; break;
		case "September" : month = 9; break;
		case "October" : month = 10; break;
		case "November" : month = 11; break;
		case "December" : month = 12; break;
		default : System.out.println("Invalid input!");
		
		}
		year = Integer.parseInt(arrOfString[2]);
		day = Integer.parseInt(arrOfString[1].substring(0, 2));
	}

}
