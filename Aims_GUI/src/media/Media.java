package media;

public abstract class Media extends Object
{
	protected String title;
	
	protected String category;
	
	protected float cost;
	
	protected int id;
	
	public Media()
	{
	}
	
	public Media(String title) 
	{
		this.title = title;
	}
	
	public Media(String title, String category) 
	{
		this.title = title;
		this.category = category;
	}
	
	public Media(String title, String category, float cost) 
	{
		this.title = title;
		this.category = category;
		this.cost = cost;
	}
	


	public String getTitle() {
		return title;
	}


	


	public String getCategory() {
		return category;
	}


	


	public float getCost() {
		return cost;
	}


	
	
	public void printInfo()
	{
		
	}
	
	public boolean Equals(Media media)
	{
		return (this.id == media.id);
	}
	
	public String toString()
	{
		return title + "\n" + category;
	}
	

}
