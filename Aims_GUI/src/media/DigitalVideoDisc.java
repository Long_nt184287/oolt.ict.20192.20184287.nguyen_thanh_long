package media;

public class DigitalVideoDisc extends Disc implements Comparable
{
	
	
	public DigitalVideoDisc()
	{
		super();
	}
	public DigitalVideoDisc(String title) 
	{
		super(title);
	}
	public DigitalVideoDisc(String title, String category) 
	{
		super(title, category);
	}
	
	public DigitalVideoDisc(String title, String category, float cost) 
	{
		super(title, category, cost);
	}
	public DigitalVideoDisc(String title, String category, float cost, int lenght) 
	{
		super(title, category, cost, lenght);
	}
	public DigitalVideoDisc(String title, String category, float cost, int lenght, String director ) 
	{
		super(title, category, cost, lenght, director);
	}
	
	
	boolean search(String title)
	{
		int check = 0;
		String[] tokens = title.split(" ");
		String[] refTokens = this.title.split(" ");
		for(int i = 0; i < refTokens.length; i++)
		{
			for(int j = 0; j < tokens.length; j ++)
			{
				if(tokens[j] == refTokens[i])
				{check++;}
			}
		}
		if(check == refTokens.length)
		{
			return true;
		}
		else
		return false;
	}
	
	public void printInfo()
	{
		System.out.println(". DVD - "+ this.title + " - " + this.category + " - " + this.director + "-" + this.length + " : " + this.cost + "$" );
	}
	
	public void play()
	{
		System.out.println("Playing DVD: " + this.getTitle());
		System.out.println("DVD length: " + this.getLength());
	}
	
	public int compareTo(Object m)
	{
		return ((Float)this.getCost()).compareTo(((Media) m).getCost());
	}
	

}
