package media;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Book extends Media
{

	String content;
	public ArrayList<String> contentTokens = new ArrayList<String>();
	public Map<String, Integer> wordFrequency = new HashMap<String, Integer>();
	
	private ArrayList<String> authors = new ArrayList<String>();
	
	public void setContent(String s)
	{
		this.content = s;
	}
	
	public Book()
	{
		super();
	}
	
	public Book(String title) 
	{
		super(title);
	}
	public Book(String title,String category)
	{
		super(title, category);
	}
	public Book(String title, String category, ArrayList<String> authors)
	{
		super(title, category);
		this.authors = authors;
	}
	public Book(String title, String category, ArrayList<String> authors, float cost)
	{
		super(title, category, cost);
		this.authors = authors;
	}

	public void addAuthor(String authorName)
	{
		boolean check = false;
		if(authors.isEmpty()) 
		{
			authors.add(authorName);
		}
		else
		for(String a : authors )
		{
			if(a == authorName)
			{
				System.out.println("The author has already been in the list!");
				check = true;
				break;
			}
			
		}
		if(!check) {authors.add(authorName);}
	}
	
	public void removeAuthor(String authorName)
	{
		boolean check = false;
		int index =0;
		for(String a : authors )
		{
			if(a == authorName)
			{
				authors.remove(index);
				check = true;
				break;
			}
			index++;
		}
		if(!check) 
		{
			System.out.println("There is no such author!");
		}
	}
	
	public String printAuthors()
	{
		String listOfAuthors ="";
		for(String a : authors )
		{
			listOfAuthors += (a + ";");
			
		}
		return listOfAuthors;
	}
	
	public void printInfo()
	{
		System.out.println(". Book - "+ super.title + " - " + super.category + " - " + this.printAuthors() + " : " + super.cost + "$" );
	}

	
	public void processContent()
	{
		String[] tmpArr = content.split(" ");
		for(int i = 0; i < tmpArr.length; i++ )
		{
			contentTokens.add(tmpArr[i]);
		}
		
		Collections.sort(contentTokens);
		
		for (String tmp : contentTokens)
		{
			if(!wordFrequency.containsKey(tmp))
			{
				wordFrequency.put(tmp, 1);
			}
			
			else
			{
				int i = wordFrequency.get(tmp);
				wordFrequency.replace(tmp,i+1);
			}
		}

	}
	
	public String toString()
	{
		return "\ntitle: " + this.title + "\ncategory: " + this.category + "\nauthors: " + this.authors + "\ncontent: " + this.content + "\nfreq map:" + wordFrequency;
	}
	
	
}
