package application;
	
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;


public class Main extends Application 
{
	public static Stage pStage;
	@Override
	public void start(Stage primaryStage)
	{
		try
		{
			pStage = primaryStage;
			Parent root = FXMLLoader.load(getClass().getResource("/application/Aims_Layout.fxml"));
			primaryStage.setTitle("My Application");
			
			root.getStylesheets().add("application/application.css");
			primaryStage.setScene(new Scene(root));
			primaryStage.show();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
		
	}
}
