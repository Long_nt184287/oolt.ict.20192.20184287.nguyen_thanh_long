package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import media.Media;
import order.Order;

public class MainController implements Initializable
{
	//Do somethings?
	public void initialize(URL location, ResourceBundle resources )
	{
		
	}
	
	//stuff created in JFX for customization ~~
	public Button createNewOrder_B;
	public Button addItems_B;
	public Button displayOrders_B;
	public AnchorPane mainPane;
	public TableView<Media> table;
	public MenuButton orderListMenu;
	public TableColumn<Media, String> mediaTitle;
	public TableColumn<Media, String> mediaCategory;
	public TableColumn<Media, Float> mediaCost;
	public TableColumn<Media, Integer> mediaLength;
	public TableColumn<Media, String> mediaDirector;
	ObservableList<Media> mediaList = FXCollections.observableArrayList();
	public Button deleteItemButton;
	
	
	
	public void UpdateMediaList(ArrayList<Media> a)
	{
		mediaTitle.setCellValueFactory(new PropertyValueFactory<>("title"));
		mediaCategory.setCellValueFactory(new PropertyValueFactory<>("category"));
		mediaCost.setCellValueFactory(new PropertyValueFactory<>("cost"));
		mediaLength.setCellValueFactory(new PropertyValueFactory<>("length"));
		mediaDirector.setCellValueFactory(new PropertyValueFactory<>("director"));
	}
	
	
	public void SetTitleValue(ArrayList<Media> a)
	{
		mediaList.clear();
		UpdateMediaList(a);
		
		for(Media b : a)
		{
			mediaList.add(b);
		}
		table.setItems(mediaList);
	
	}
	
	
	public void DeleteOption()
	{
		table.setOnMouseClicked(evt -> 
		{
			int index = table.getSelectionModel().getSelectedCells().get(0).getRow();
			System.out.println(index);
			deleteItemButton.setVisible(true);
			deleteItemButton.setOnAction(evt2 -> 
				{
					om.GetCurrentOrder().removeMedia(index);
					
					deleteItemButton.setVisible(false);
					SetTitleValue(om.GetCurrentOrder().getItemsOrdered());
				});
			
			
		});
	}
	
	
	
	//current instance of the program!
	OrderManager om = new OrderManager();
	
	
	// allow customer to choose an order?? 
	public void ShowOrderList()
	{
		orderListMenu.setVisible(true);
		orderListMenu.getItems().clear();
		
		ArrayList<Order> tmp = om.getOrderList();
		int i = 1;
		for(Order o : tmp)
		{
			String a = "Order" + i;
			
			MenuItem newMenuItem = new MenuItem(a);
			newMenuItem.setId(i + "");
			
			//list the order the customer has created, and add event to display them on tableview!
			newMenuItem.setOnAction(evt -> 
				{
					om.setListIndex(Integer.parseInt(newMenuItem.getId())-1);
					SetTitleValue(o.getItemsOrdered());
					DeleteOption();
					
					
				});
			orderListMenu.getItems().add(newMenuItem);
			i++;
		}
	}
	
	
	
	public void AddItems()
	{
		
		om.ShowOrderInterface();
	}
	
	public void CreateNewOrder()
	{
		om.CreateNewOrder();
	}
	
	
	
}
