package application;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import media.*;
import order.Order;

public class OrderManager implements Initializable
{
	//something?
	public void initialize(URL location, ResourceBundle resources )
	{
		
	}
	
	
	//list of orders!
	private ArrayList<Order> orderList = new ArrayList<Order>();
	
	
	//get order list to create list of orders of user!
	public ArrayList<Order> getOrderList()
	{
		return orderList;
	}
	
	public Order GetCurrentOrder()
	{
		return orderList.get(currentListIndex);
	}
	
	
	//set up shoping items for customer!
	private ArrayList<Media> AvailbleItems = new ArrayList<Media>();
	
	public OrderManager()
	{
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation",19.95f, 87,  "Roger Allers");
		DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction", 24.95f, 124, "George Lucas");
		DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladin","Animation",18.99f,90,"John Musker");
		
		ArrayList<String> myAuthor = new ArrayList<String>();
		myAuthor.add("Someone1");
		myAuthor.add("Someone2");
		
		Book book1 = new Book("Little Prince", "Telltale", myAuthor, 33.33f);
		
		
		AvailbleItems.add(dvd1);
		AvailbleItems.add(dvd2);
		AvailbleItems.add(dvd3);
		AvailbleItems.add(book1);
	}
	
	
	
	
	
	//keep track of current order in the list of orders!
	private int currentListIndex = 0;
	
	
	public void setListIndex(int i)
	{
		currentListIndex = i;
	}
	
	
	
	public void CreateNewOrder()
	{
		Order newOrder = new Order();
		orderList.add(newOrder);
		currentListIndex = orderList.size() - 1;
	}
	
	
	
	public void ShowOrderInterface()
	{
		try
		{
			AnchorPane orderMenu = FXMLLoader.load(getClass().getResource("/application/Order_Layout.fxml"));
			
			Stage orderWindow = new Stage();
			orderWindow.setTitle("Order");
			
			orderMenu.getStylesheets().add("application/application.css");
			orderWindow.setScene(new Scene(orderMenu));
			
			//get tilepane insides orderMenu
			TilePane tp = (TilePane)(orderMenu.getChildren().get(0));
			
			//set up button and event when clicking at item!
			for(Media a : AvailbleItems)
			{
				Button button = new Button(a.toString());
				button.setOnAction(evt -> {orderList.get(currentListIndex).addMedia(a); System.out.println(a.toString());});
				button.setPrefSize(100, 100);
				tp.getChildren().add(button);
			}
			
			orderWindow.initModality(Modality.WINDOW_MODAL);
			orderWindow.initOwner((Stage)Main.pStage.getScene().getWindow());
			
			
			
			
			orderWindow.show();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	
	public void ConfirmOrder()
	{
	}

}
